'use strict';

module.exports = function(app) {
	  // Migrate
  var migrateAndUpdate = function () {
    const numModels = Object.keys(app.models).length
    for (let dataSource of Object.values(app.dataSources)) {
      dataSource.setMaxListeners(numModels)
    }
    var db = app.dataSources.mysqlDev;
    var appModels = Object.keys(db.connector._models)

    db.isActual(appModels, function (err, actual) {
      if (!actual) {
        db.autoupdate(appModels, function (err, result) {
          if (err) throw err;
          console.log('updated')
        });
      } else {
        console.log("Already Up to date, Seeding...")
      }
    });
  }

  migrateAndUpdate()
};