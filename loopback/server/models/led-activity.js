'use strict';
var moment = require('moment');

module.exports = function(LedActivity) {

	LedActivity.getMyActivities = function ( callback) {
        let filter = { 

            where : {  isDone : false }
        }

        LedActivity.find( filter, (error, activities)  => {
            if (error) return callback(error);

        	LedActivity.updateAll( { isDone : false}, { isDone : true}, (error, done)  => {
            if (error) return callback(error);
    
	            return callback(null, activities);
			});
		});

    }

};
