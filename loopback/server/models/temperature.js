'use strict';
var moment = require('moment');
var momentTZ = require('moment-timezone');
const TIME_ZONE = 'America/Mexico_City';

module.exports = function(Temperature) {

	Temperature.createOne = function ( data, callback) {

        let newLecture = { 
            value : data.celsius,
            date : moment.utc()
        }

        Temperature.create( newLecture, (error, created)  => {
            if (error) return callback(error);

 
	            return callback(null, true);
		});

    }


	Temperature.test = function ( hour, days, temperature, callback) {
		let date = moment.utc().set({'hour': hour});
        let newLecture = { 
            value : temperature,
            date : moment(hour, 'HH:mm').subtract(days, 'days')
        }
        console.log(newLecture)
        Temperature.create( newLecture, (error, created)  => {
            if (error) return callback(error);

 
	            return callback(null, true);
		});

    }

    Temperature.getOne = function ( filterWord, callback) {
    	if(filterWord == undefined || filterWord == "undefined"){
    		filterWord = 'now'
    	}
    	let dayTimes = { morning : 14 , afternoon : 17, evening  : 23, night  :2} //9
    	let filter = {}
    	let nowDateUTC = moment.utc();
    	let day = null;
    	let passed = true;
    	switch(filterWord) {
		  	case 'morning':
		    case 'afternoon':    		
		    case 'evening':
    			day =   moment.utc().set({'hour': dayTimes[filterWord]});
    			passed = nowDateUTC.isAfter(moment(day))
		    	if(!passed){
    				day =   moment(day).subtract(1, 'days');
		    	}
		  		filter.where = { date : { lt : day }}
			  	filter.order = 'id DESC';
			  	filter.limit = 1;
    		break;
		  	case 'night':
		  		day =   moment.utc().set({'hour': dayTimes[filterWord]}).add(1, 'days');
    			passed = nowDateUTC.isAfter(moment(day))
		    	if(!passed){
    				day =   moment(day).subtract(1, 'days');
		    	}
		  		filter.where = { date : { lt : day }}
			  	filter.order = 'id DESC';
			  	filter.limit = 1;
		    break;
		    case 'yesterday':
		    	day = moment.utc().set({'hour': 0}).subtract(1, 'days');
		  		filter.where = { date : { lt : day }}
			  	filter.order = 'id DESC';
			  	filter.limit = 1;
		    break;
		  	case 'today':
		  	default:
			  	filter.limit = 1;
			  	filter.order = 'id DESC';

		}
		    	console.log(filterWord, day)

        Temperature.find(filter, (error, temperature)  => {
            if (error) return callback(error);
            	let speech = "";
 				if(temperature.length == 0){
 					speech = "Sorry, i couldn't find any value at that time"
 					return callback(null, speech);
 				}

 			let value = temperature[0].value
 			switch(filterWord) {
			    case 'afternoon':
			    case 'evening':
			  	case 'morning':
			  	case 'night':
			  		if(passed)
	    				speech = "the temperature in the " + filterWord +" was " + value + " degrees celsius in your room";
	    			else
	    				speech = "the temperature in the " + filterWord +" yesterday was " + value + " degrees celsius in your room";
			    break;
			    case 'yesterday':
	    			speech = "the temperature yesterday was " + value + " degrees celsius in your room";
			    break;
			  	case 'today':
			  	default:
	    			speech = "the temperature " + filterWord +" is " + value + " degrees celsius in your room";

			}
			console.log(filterWord, ' ', Number(value))
			let speechAddition = "";
			if(Number(value) <= 7){
            	speechAddition = '<amazon:emotion name="excited" intensity="medium"> It is frozen!</amazon:emotion>';	
 			}
			if(Number(value) <= 16 && Number(value) >= 8){
            	speechAddition = ' It is cold, i call it sweater weather';	
 			}
 			if(Number(value) >= 17 && Number(value) <= 30){
            	speechAddition = '<amazon:emotion name="excited" intensity="medium"> That is a great weather </amazon:emotion>';	
 			}

 			if(Number(value) >= 31  && Number(value) <= 33){
            	speechAddition = ' It is warm, like a summer weather';	
 			}
 			if( Number(value) >= 34){
            	speechAddition = '<amazon:emotion name="disappointed" intensity="high"> Oh my god, that is very hot</amazon:emotion>';	
 			}

	        return callback(null, speech + speechAddition);
		});

    }

};
