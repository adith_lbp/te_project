#! /usr/bin/python

from gpiozero import TrafficLights
from time import sleep
import requests
import json
import serial
from datetime import datetime, timezone

ser = serial.Serial("/dev/ttyACM0", 9600)
ser.flushInput()

lights = TrafficLights(25, 8, 7)
def resolveActivity(activity):
        if(activity["name"] == "red"):
                if(activity["action"]):
                        lights.red.on()
                else:
                        lights.red.off()
        if(activity["name"] == "yellow"):
                if(activity["action"]):
                        lights.amber.on()
                else:
                        lights.amber.off()
        if(activity["name"] == "green"):
                if(activity["action"]):
                        lights.green.on()
                else:
                        lights.green.off()
        if(activity["name"] == "all"):
                if(activity["action"]):
                        lights.red.on()
                        lights.green.on()
                        lights.amber.on()
                else:
                        lights.green.off()
                        lights.red.off()
                        lights.amber.off()
def getActivities():
        #print ('here')
        #print (r.status_code)  # Status code of response

        r = requests.get("https://smartberry.gq/loopback/LedActivities/current")
        if(r.status_code == 200):
                data = r.content  # Content of response
                return json.loads(data)

def readTemperature():
        try:
                lineBytes = ser.readline()
                line = lineBytes.decode("utf-8").strip()
                #print(line)
                return line
        except KeyboardInterrupt:
                print("problem getting serial")
                return False

def sendTemperature(temperature):
        date = datetime.now()
        print(date, temperature)
        r = requests.post("https://smartberry.gq/loopback/Temperatures/", data={'value': temperature, 'date' : date })
        print(r.status_code, r.reason)

count = 0
while True:
        #print("count ", count)
        activities = getActivities()
        print("Activities ", len(activities))
        for item in activities:
                resolveActivity(item)
                sleep(1)
        if(count == 4):
                temperature = readTemperature()
                if(temperature):
                        sendTemperature(temperature)
                count = 0
        count +=1
        sleep(2)